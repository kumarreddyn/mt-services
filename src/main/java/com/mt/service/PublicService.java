package com.mt.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mt.common.EncryptDecryptUtil;
import com.mt.common.ResponseUtil;
import com.mt.constants.DataConstants;
import com.mt.constants.ServiceConstants;
import com.mt.dto.LoginDTO;
import com.mt.dto.UserDTO;
import com.mt.entity.User;
import com.mt.security.JWTHelper;

@Service
public class PublicService {
	
	private final Logger logger = LoggerFactory.getLogger(PublicService.class);
	private final ResponseUtil responseUtil;
	private final JWTHelper jwtHelper;
	private final UserService userService;
	private final EncryptDecryptUtil encryptDecryptUtil;
	
	public PublicService(ResponseUtil responseUtil, JWTHelper jwtHelper, 
			UserService userService, EncryptDecryptUtil encryptDecryptUtil) {
		this.responseUtil = responseUtil;
		this.jwtHelper = jwtHelper;
		this.userService = userService;
		this.encryptDecryptUtil = encryptDecryptUtil;
	}

	public ResponseEntity<Object> checkServicesStatus() {
		Map<String, Object> dataMap = new HashMap<>();		
		return responseUtil.generateResponse(dataMap, ServiceConstants.REST_SERVICES_ACCESSIBLE);
	}
	
	public ResponseEntity<Object> register(UserDTO userDTO) {
		Map<String, Object> dataMap = new HashMap<>();
		Optional<User> userOptional = Optional.empty();
		try {
			User user = userService.toUser(userOptional, userDTO);
			user.setActive(true);
			user.setCreatedDate(new Date());
			user = userService.save(user);
			dataMap.put(DataConstants.USER, userService.toUserDTO(user));		
			return responseUtil.generateResponse(dataMap, ServiceConstants.USER_REGISTERED);
		}catch (Exception e) {
			logger.error("Not able to register the user. {}", e.getMessage());
			return responseUtil.generateResponse(dataMap, ServiceConstants.USER_NOT_REGISTERED);
		}
	}

	public ResponseEntity<Object> login(LoginDTO loginDTO) {
		Map<String, Object> dataMap = new HashMap<>();
		Optional<User> userOptional = userService.findByEmailAddress(loginDTO.getEmailAddress());
		if(userOptional.isPresent()) {
			User user = userOptional.get();
			if(user.getPassword() != null 
					&& encryptDecryptUtil.decrypt(user.getPassword()).equals(loginDTO.getPassword())) {
				String authToken = jwtHelper.generateAuthenticationToken(user);		
				dataMap.put(DataConstants.X_AUTH_TOKEN, authToken);
				return responseUtil.generateResponse(dataMap, ServiceConstants.USER_AUTHENTICATION_VALID);
			}
		}
		return responseUtil.generateResponse(dataMap, ServiceConstants.USER_AUTHENTICATION_INVALID);	
	}

}
