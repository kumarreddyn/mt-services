package com.mt.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mt.common.ResponseUtil;
import com.mt.common.SecurityUtil;
import com.mt.constants.DataConstants;
import com.mt.constants.FileConstants;
import com.mt.constants.SecurityConstants;
import com.mt.constants.ServiceConstants;
import com.mt.dto.UserDTO;
import com.mt.dto.converter.UserConverter;
import com.mt.dto.repository.UserRepository;
import com.mt.entity.User;



@Service
public class UserService {

	private final Logger logger = LoggerFactory.getLogger(UserService.class);
	private final UserRepository userRepository;
	private final UserConverter userConverter;
	private final ResponseUtil responseUtil;
	private final SecurityUtil securityUtil;
	private final FileManagerService fileManagerService;
	
	public UserService(UserRepository userRepository, UserConverter userConverter,
			ResponseUtil responseUtil, SecurityUtil securityUtil,
			FileManagerService fileManagerService) {
		this.userRepository = userRepository;
		this.userConverter = userConverter;
		this.responseUtil = responseUtil;
		this.securityUtil = securityUtil;
		this.fileManagerService = fileManagerService;
	}

	public User save(User user) {
		return userRepository.save(user);
	}

	public User toUser(Optional<User> userOptional, UserDTO userDTO) {
		return userConverter.toUser(userOptional, userDTO);
	}
	
	public UserDTO toUserDTO(User user) {
		return userConverter.toUserDTO(user);
	}
	
	public List<UserDTO> toUserDTOList(Set<User> users) {
		return userConverter.toUserDTOList(users);
	}
	
	public Optional<User> findByUserId(String userId){
		return userRepository.findByUserId(userId);
	}
	
	public Optional<User> findByEmailAddress(String emailAddress){
		return userRepository.findByEmailAddress(emailAddress);
	}
	
	public ResponseEntity<Object> save(HttpServletRequest request, UserDTO userDTO, MultipartFile photo) {
		Map<String, Object> dataMap = new HashMap<>();
		Optional<User> userOptional = Optional.empty();
		try {
			User user = userConverter.toUser(userOptional, userDTO);
			user.setActive(true);
			user.setCreatedDate(new Date());
			user.setCreatedBy(securityUtil.getLongValueFromRequest(request, SecurityConstants.LOGGED_IN_USER_ID));
			user = save(user);
			if(null != photo) {
				String filePath = getFileUploadPath(user, FileConstants.FOLDER_PHOTOS);
				String photoURL = fileManagerService.uploadFile(filePath, photo);
				//user.setPhotoURL(photoURL);
			}
			dataMap.put(DataConstants.USER, toUserDTO(user));		
			return responseUtil.generateResponse(dataMap, ServiceConstants.USER_SAVED);
		}catch (DuplicateKeyException e) {
			return responseUtil.generateResponse(dataMap, ServiceConstants.USER_ALREADY_EXIST);
		}catch (Exception e) {
			logger.error("Not able to save the user. {}", e.getMessage());
			return responseUtil.generateResponse(dataMap, ServiceConstants.USER_NOT_SAVED);
		}
	}
	
	private String getFileUploadPath(User user, String folderName) {
		return FileConstants.FILE_PATH_SEPERATOR + FileConstants.FOLDER_USERS + FileConstants.FILE_PATH_SEPERATOR 
				+ user.getUserId() + FileConstants.FILE_PATH_SEPERATOR 
				+ folderName + FileConstants.FILE_PATH_SEPERATOR;
	}

	public ResponseEntity<Object> update(HttpServletRequest request, UserDTO userDTO, MultipartFile photo) {
		Map<String, Object> dataMap = new HashMap<>();
		try {
			Optional<User> userOptional = userRepository.findById(userDTO.getUserId());
			if(userOptional.isPresent()) {
				User user = userConverter.toUser(userOptional, userDTO);
				user.setModifiedDate(new Date());
				user.setModifiedBy(securityUtil.getLongValueFromRequest(request, SecurityConstants.LOGGED_IN_USER_ID));
				if(null != photo) {
					String filePath = getFileUploadPath(user, FileConstants.FOLDER_PHOTOS);
					String photoURL = fileManagerService.uploadFile(filePath, photo);
					//user.setPhotoURL(photoURL);
				}
				user = save(user);
				dataMap.put(DataConstants.USER, toUserDTO(user));		
				return responseUtil.generateResponse(dataMap, ServiceConstants.USER_UPDATED);
			}else {
				return responseUtil.generateResponse(dataMap, ServiceConstants.USER_NOT_FOUND);
			}
		}catch (Exception e) {
			logger.error("Not able to update the user. {}", e.getMessage());
			return responseUtil.generateResponse(dataMap, ServiceConstants.USER_NOT_UPDATED);
		}
	}

	public ResponseEntity<Object> delete(HttpServletRequest request, UserDTO userDTO) {
		Map<String, Object> dataMap = new HashMap<>();
		try {
			Optional<User> userOptional = userRepository.findById(userDTO.getUserId());
			if(userOptional.isPresent()) {
				User user = userConverter.toUser(userOptional, userDTO);
				user.setActive(false);
				user.setModifiedDate(new Date());
				user.setModifiedBy(securityUtil.getLongValueFromRequest(request, SecurityConstants.LOGGED_IN_USER_ID));
				save(user);
				return responseUtil.generateResponse(dataMap, ServiceConstants.USER_DELETED);
			}else {
				return responseUtil.generateResponse(dataMap, ServiceConstants.USER_NOT_FOUND);
			}
		}catch (Exception e) {
			logger.error("Not able to delete the user. {}", e.getMessage());
			return responseUtil.generateResponse(dataMap, ServiceConstants.USER_NOT_DELETED);
		}
	}

	public ResponseEntity<Object> getAllActiveUsers() {
		Map<String, Object> dataMap = new HashMap<>();
		Set<User> users = userRepository.findByActiveOrderByNameAsc(true);
		dataMap.put(DataConstants.USER_LIST, toUserDTOList(users));
		return responseUtil.generateResponse(dataMap, ServiceConstants.USER_LIST_FOUND);
	}

}
