package com.mt.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mt.constants.SecurityConstants;
import com.mt.entity.User;
import com.mt.service.UserService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTHelper {
	
	@Value("${jwt.secret.key}") 
	private String jwtSecretKey;
	
	private final UserService userService;
	
	public JWTHelper(UserService userService) {
		this.userService = userService;
	}
	
	public String generateAuthenticationToken(User user) {
		Long now = System.currentTimeMillis();
		return Jwts.builder()
			.setSubject(user.getEmailAddress())
			.addClaims(generateCustomClaims(user))
			.setIssuedAt(new Date(now))
			.setExpiration(new Date(now + (24 * 60 * 60 * 1000)))  //1 day
			.signWith(SignatureAlgorithm.HS512, jwtSecretKey.getBytes())
			.compact();
	}
	
	public Claims decodeToken(String authToken){
		Claims claims = null;
		if (authToken != null) {
			claims = Jwts.parser().setSigningKey(jwtSecretKey.getBytes()).parseClaimsJws(authToken)
					.getBody();
		}
		return claims;
	}

	public String generateRefreshToken(Claims claims) {
		if(null != claims.get(SecurityConstants.LOGGED_IN_USER_ID)) {
			Optional<User> userOptional = userService.findByUserId(String.valueOf(claims.get(SecurityConstants.LOGGED_IN_USER_ID)));
			if(userOptional.isPresent()) {
				return generateAuthenticationToken(userOptional.get());
			}
		}
		return null;
	}
	
	private Map<String, Object> generateCustomClaims(User user) {
		Map<String, Object> customClaims = new HashMap<>();
		customClaims.put(SecurityConstants.LOGGED_IN_USER_ID, user.getUserId());
//		if(null != user.getRoles()) {
//			Set<String> userRoles = user.getRoles().stream().map(Role::getCode).collect(Collectors.toSet());
//			customClaims.put(SecurityConstants.USER_ROLES, userRoles);
//		}
		return customClaims;
	}
}
