package com.mt.security;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.mt.constants.DataConstants;
import com.mt.constants.SecurityConstants;

import io.jsonwebtoken.Claims;

@Component
public class RestAPIAuthenticationFilter extends OncePerRequestFilter {

	private final JWTHelper jwtHelper;
	
	@Autowired
	public RestAPIAuthenticationFilter(JWTHelper jwtHelper) {
		this.jwtHelper = jwtHelper;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String authToken = request.getHeader(DataConstants.X_AUTH_TOKEN);
		Claims claims = jwtHelper.decodeToken(authToken);
		if (null != claims) {
			Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
			Object userRoles = claims.get(SecurityConstants.USER_ROLES); 
			if(userRoles != null) {
				@SuppressWarnings("unchecked")
				List<String> roles = ((List<String>) userRoles);
				grantedAuthorities = roles.stream().map(SimpleGrantedAuthority::new
				).collect(Collectors.toSet());
			}else {
				GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_USER");
				grantedAuthorities.add(grantedAuthority);
			}
			
			Authentication auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null, grantedAuthorities);
			SecurityContextHolder.getContext().setAuthentication(auth);
			if(null != claims.get(SecurityConstants.LOGGED_IN_USER_ID)) {
				request.setAttribute(SecurityConstants.LOGGED_IN_USER_ID, claims.get(SecurityConstants.LOGGED_IN_USER_ID));
			}
			response.setHeader(DataConstants.REFRESH_TOKEN, getRefreshToken(claims));
			filterChain.doFilter(request, response);
		} else {
			throw new SecurityException("Bad Credentials");
		}
	}

	private String getRefreshToken(Claims claims) {
		String refreshToken = null;
		Date currentDate = new Date();
		long currentTime =  currentDate.getTime();
		long expiryTime = claims.getExpiration().getTime();
		long diffMinutes = (expiryTime - currentTime) / (60 * 1000);		
		if(diffMinutes < SecurityConstants.JWT_REFRESH_TOKEN_TIME_MINS) {
			refreshToken = jwtHelper.generateRefreshToken(claims);
		}
		return refreshToken;
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		String path = request.getServletPath();
		return (path.contains("/public/") || path.contains("/file-manager/"));
	}

}
