package com.mt.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mt.dto.LoginDTO;
import com.mt.dto.UserDTO;
import com.mt.service.PublicService;

@RestController
@RequestMapping(path = "/public")
public class PublicController {
	
	private final PublicService publicService;
	
	PublicController(PublicService publicService){
		this.publicService = publicService;
	}

	@GetMapping(path = "/services-status", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> checkServicesStatus() {
		return publicService.checkServicesStatus();
	}
	
	@PostMapping(path = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> register(@RequestBody UserDTO userDTO) {
		return publicService.register(userDTO);
	}
	
	@PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> login(@RequestBody LoginDTO loginDTO) {
		return publicService.login(loginDTO);
	}
	
}
