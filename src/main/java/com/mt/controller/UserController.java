package com.mt.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mt.dto.UserDTO;
import com.mt.service.UserService;

@RestController
@RequestMapping(path = "/user")
public class UserController {

	private final UserService userService;
	private final ObjectMapper objectMapper;
	
	public UserController(UserService userService, ObjectMapper objectMapper) {
		this.userService = userService;
		this.objectMapper = objectMapper;
	}
	
	//@PreAuthorize("hasRole('"+RoleConstants.ADMIN+"')")
	@PostMapping(path = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> save(HttpServletRequest request, @RequestParam String user,
			@RequestParam(name="photo", required=false) MultipartFile photo) throws JsonMappingException, JsonProcessingException {
		final UserDTO userDTO = objectMapper.readValue(user, UserDTO.class);
		return userService.save(request, userDTO, photo);
	}
	
	@PostMapping(path = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> update(HttpServletRequest request, @RequestParam String user,
			@RequestParam(name="photo", required=false) MultipartFile photo) throws JsonMappingException, JsonProcessingException {
		final UserDTO userDTO = objectMapper.readValue(user, UserDTO.class);
		return userService.update(request, userDTO, photo);
	}
	
	@PostMapping(path = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> delete(HttpServletRequest request, @RequestBody UserDTO userDTO) {
		return userService.delete(request, userDTO);
	}
	
	@GetMapping(path = "/get-all-users", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAllActiveUsers() {
		return userService.getAllActiveUsers();
	}
	
}
