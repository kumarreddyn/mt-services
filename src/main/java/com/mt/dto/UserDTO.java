package com.mt.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {

	private Long userId;
	private String name;
	private String emailAddress;
	private String mobileNumber;
	private String password;	
	
}
