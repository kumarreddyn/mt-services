package com.mt.dto.converter;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.mt.common.EncryptDecryptUtil;
import com.mt.dto.UserDTO;
import com.mt.entity.User;

@Component
public class UserConverter {

	private final EncryptDecryptUtil encryptDecryptUtil;

	public UserConverter(EncryptDecryptUtil encryptDecryptUtil) {
		this.encryptDecryptUtil = encryptDecryptUtil;
	}
	
	public User toUser(Optional<User> userOptional, UserDTO userDTO) {
		User user = new User();
		if(userOptional.isPresent()) {
			user = userOptional.get();
		}
		user.setName(userDTO.getName());
		user.setEmailAddress(userDTO.getEmailAddress());
		user.setMobileNumber(userDTO.getMobileNumber());
		if(userDTO.getPassword() != null && !userDTO.getPassword().isEmpty()) {
			user.setPassword(encryptDecryptUtil.encrypt(userDTO.getPassword()));	
		}
		return user;
	}
	
	public UserDTO toUserDTO(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setUserId(user.getUserId());
		userDTO.setName(user.getName());
		userDTO.setEmailAddress(user.getEmailAddress());
		userDTO.setMobileNumber(user.getMobileNumber());
		return userDTO;
	}

	public List<UserDTO> toUserDTOList(Set<User> users) {
		return users.stream().sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
		.map(this::toUserDTO).collect(Collectors.toList());
	}
	
}
