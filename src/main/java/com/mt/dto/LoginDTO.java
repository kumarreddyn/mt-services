package com.mt.dto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDTO {

	private String emailAddress;
	private String password;
	
}