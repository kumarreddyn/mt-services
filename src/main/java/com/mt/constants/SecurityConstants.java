package com.mt.constants;

public class SecurityConstants {

	private SecurityConstants() {
	}
	
	public static final String LOGGED_IN_USER_ID = "loggedInUserId";
	public static final String USER_ROLES = "userRoles";
	public static final long JWT_REFRESH_TOKEN_TIME_MINS = 60;

}
