package com.mt.constants;

public class DataConstants {

	private DataConstants() {
	}
	
	public static final String X_AUTH_TOKEN = "X_AUTH_TOKEN";
	public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
	
	public static final String CODE = "code";
	public static final String DATA = "data";
	
	public static final String USER = "user";
	public static final String USER_LIST = "userList";
	
}
