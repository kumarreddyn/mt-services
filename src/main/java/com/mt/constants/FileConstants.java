package com.mt.constants;

public class FileConstants {

	private FileConstants() {
	}

	public static final String FILE_PATH_SEPERATOR = "/";
	public static final String FOLDER_USERS = "users";
	public static final String FOLDER_PHOTOS = "photos";

}
