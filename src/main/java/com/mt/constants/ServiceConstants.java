package com.mt.constants;

public class ServiceConstants {

	private ServiceConstants() {		
	}
	
	public static final String REST_SERVICES_ACCESSIBLE = "REST_SERVICES_ACCESSIBLE";	
	public static final String USER_AUTHENTICATION_VALID = "USER_AUTHENTICATION_VALID";
	public static final String USER_AUTHENTICATION_INVALID = "USER_AUTHENTICATION_INVALID";
	
	public static final String USER_REGISTERED = "USER_REGISTERED";
	public static final String USER_NOT_REGISTERED = "USER_NOT_REGISTERED";
	
	public static final String USER_SAVED = "USER_SAVED";
	public static final String USER_UPDATED = "USER_UPDATED";
	public static final String USER_DELETED = "USER_DELETED";
	public static final String USER_NOT_SAVED = "USER_NOT_SAVED";
	public static final String USER_NOT_UPDATED = "USER_NOT_UPDATED";
	public static final String USER_NOT_DELETED = "USER_NOT_DELETED";	
	public static final String USER_FOUND = "USER_FOUND";
	public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
	public static final String USER_LIST_FOUND = "USER_LIST_FOUND";
	public static final String USER_ALREADY_EXIST = "USER_ALREADY_EXIST";

}
